﻿using GOFCuttingShape.Models;

namespace Composite
{
    class Program
    {
        public static async Task Main()
        {
            Form xml = new Form("testForm");
            InputText inputText = new InputText("textName", "textValue");
            LabelText labelText = new LabelText( "LabelText");
            InputText inputText2 = new InputText("textName2", "textValue2");
            xml.AddComponent(inputText);
            xml.AddComponent(labelText);
            xml.AddComponent(inputText2);
            Console.WriteLine(xml.ConvertToString());
            Console.WriteLine(inputText.ConvertToString());
            Console.WriteLine(labelText.ConvertToString());
          
        }
    }
}
