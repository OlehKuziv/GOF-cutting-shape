using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace GOFCuttingShape.Services;

public class CustomXmlSerializer 
{
    public T Deserizalize<T>(string data)
    {
        var result = default(T);
        if (string.IsNullOrEmpty(data))
            return result;

        using (var textReader = new StringReader(data))
        {
            try
            {
                var serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
                result = (T) serializer.Deserialize(textReader);
            }
            catch
            {
            }
        }

        return result;
    }
    
    // This also works but as I understand not always https://stackoverflow.com/questions/31946240/remove-q1-and-all-namespaces-from-xml#31947478
    public string SerializeObject( object obj)
    {
        var ns = new XmlSerializerNamespaces();
        ns.Add("", "");
        var utf8NoBom = new UTF8Encoding(false);
        var settings = new XmlWriterSettings
        {
            OmitXmlDeclaration = true,
            Indent = true,
            Encoding = Encoding.UTF8
        };
        using (MemoryStream ms = new MemoryStream())
        {
            using (var xmlWriter = XmlWriter.Create(ms, settings))
            {
                XmlSerializer xmlSer = new XmlSerializer(obj.GetType());
                xmlSer.Serialize(xmlWriter, obj, ns);
                byte[] bytes = ms.ToArray();
                return utf8NoBom.GetString(bytes);
            }
        }
    }
    
    public string PostProcessXml( string rawXml)
    {
        var doc = XDocument.Parse(rawXml);

        doc.Descendants().Attributes().Where(a => a.IsNamespaceDeclaration).Remove();

        foreach (var element in doc.Descendants())
        {
            element.Name = element.Name.LocalName;
        }

        var xmlWithoutNamespaces = doc.ToString();
        return xmlWithoutNamespaces;
    }
    
    public string Serialize(object data)
    {
        if (data == null)
            throw new ArgumentNullException(nameof(data));
        var memoryStream = new MemoryStream();
        using (var writer = new StreamWriter(memoryStream, System.Text.Encoding.UTF8))
        {
            var formatter = new System.Xml.Serialization.XmlSerializer(data.GetType());
            formatter.Serialize(writer, data);
            memoryStream.Seek(0, SeekOrigin.Begin);
            var streamReader = new StreamReader(memoryStream, System.Text.Encoding.UTF8);
            return streamReader.ReadToEnd();
        }
    }
}