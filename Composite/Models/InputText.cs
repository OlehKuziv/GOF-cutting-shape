using System.Xml.Serialization;

namespace GOFCuttingShape.Models;

[XmlRoot("inputText")]
public class InputText:XmlElement
{
    [XmlAttribute("name")]
    public string Name { get; set; }
    [XmlAttribute("value")]
    public string Value { get; set; }
    public InputText(string name, string value)
    {
        Name = name;
        Value = value;
    }
    public InputText()
    {
    }
}