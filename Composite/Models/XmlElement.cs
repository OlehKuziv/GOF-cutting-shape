using GOFCuttingShape.Services;

namespace GOFCuttingShape.Models;

public abstract class XmlElement
{
    public virtual string ConvertToString()
    {
        var serializer = new CustomXmlSerializer();
        string xml =  serializer.Serialize(this);
        return serializer.PostProcessXml(xml);
    }
}