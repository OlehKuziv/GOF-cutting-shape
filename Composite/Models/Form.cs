using System.Xml.Linq;
using System.Xml.Serialization;

namespace GOFCuttingShape.Models;

[XmlRoot("form")]
public class Form:XmlElement
{
    private IList<XmlElement> _formElements = new List<XmlElement>();
    
    [XmlAttribute("name")]
    public string Name { get; set; }
    
    public Form(String name)
    {
        Name = name;
    }
    
    public Form()
    {
    }

    public void AddComponent(XmlElement xmlElement)
    { 
        _formElements.Add(xmlElement);
    }

    public override string ConvertToString()
    {
        string formXml = base.ConvertToString();
        XDocument xDocumentRoot = XDocument.Parse(formXml);
        
        foreach (var formElement in _formElements)
        {
            string xmlElement = formElement.ConvertToString();
            XDocument xDocumentElement = XDocument.Parse(xmlElement);
            xDocumentRoot.Root.Add(xDocumentElement.Root);
        }
        return xDocumentRoot.ToString();
    } 
}