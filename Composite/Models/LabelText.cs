using System.Xml.Serialization;

namespace GOFCuttingShape.Models;

[XmlRoot("labelText")]
public class LabelText:XmlElement
{
    [XmlAttribute("value")]
    public string Value { get; set; }
    public LabelText(string value)
    {
        Value = value;
    } 
    
    public LabelText()
    {
    }
}