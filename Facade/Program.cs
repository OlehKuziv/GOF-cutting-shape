﻿using Facade.Services;

namespace Facade
{
    class Program
    {
        public static async Task Main()
        {
            OrderFaçade orderFaçade = new OrderFaçade(new InvoiceSystem(), new ProductCatalog(), new PaymentSystem());
            orderFaçade.PlaceOrder(Guid.NewGuid().ToString(),3,"example.com");
        }
    }
}