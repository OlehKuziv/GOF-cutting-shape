namespace Facade;

public interface IOrderFaçade
{
    void PlaceOrder(string productId, int quantity, string email);
}