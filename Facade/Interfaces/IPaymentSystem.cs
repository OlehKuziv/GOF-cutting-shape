using Facade.Models;

namespace Facade;

public interface IPaymentSystem
{
    bool MakePayment(Payment payment); 
}