using Facade.Models;

namespace Facade;

public interface IProductCatalog
{
    Product GetProductDetails(string productId); 
}