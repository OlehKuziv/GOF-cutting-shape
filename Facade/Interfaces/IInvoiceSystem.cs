using Facade.Models;

namespace Facade;

public interface IInvoiceSystem
{
    void SendInvoice(Invoice invoice); 
}