namespace Facade.Models;

public class Payment
{
    private decimal ItemPrice;

    public Payment(string productId,int quantity, decimal price)
    {
        ItemPrice = price;
        Quantity = quantity;
        ProductId = productId;
    }

    public decimal TotalPrice => ItemPrice * Quantity;
    public string ProductId { get; }
    public int Quantity { get;} 
}