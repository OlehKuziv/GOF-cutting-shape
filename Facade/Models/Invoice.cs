namespace Facade.Models;

public class Invoice
{
    public Invoice(decimal totalPrice, string email)
    {
        TotalPrice = totalPrice;
        Email = email;
    }

    public decimal TotalPrice { get; }
    public string Email { get; }
}