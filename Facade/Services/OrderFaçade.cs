using Facade.Models;

namespace Facade;

public class OrderFaçade:IOrderFaçade
{
    private readonly IInvoiceSystem _invoiceSystem;
    private readonly IProductCatalog _productCatalog;
    private readonly IPaymentSystem _paymentSystem;

    public OrderFaçade(IInvoiceSystem invoiceSystem, IProductCatalog productCatalog, IPaymentSystem paymentSystem)
    {
        _invoiceSystem = invoiceSystem;
        _productCatalog = productCatalog;
        _paymentSystem = paymentSystem;
    }
    public void PlaceOrder(string productId, int quantity, string email)
    {
        Product product = _productCatalog.GetProductDetails(productId);
        Payment payment = new Payment(productId, quantity, product.Price);
        bool paymentSuccessful = _paymentSystem.MakePayment(payment);
        Invoice invoice = new Invoice(payment.TotalPrice, email);
        _invoiceSystem.SendInvoice(invoice);
    }
}