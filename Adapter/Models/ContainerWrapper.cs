using Adapter._3partyLib;
using Adapter.Interfaces;

namespace Adapter.Models;

public class ContainerWrapper<T>:IContainer<T>
{
    private readonly IElements<T> _classToPrint;

    public ContainerWrapper(IElements<T>  classToPrint)
    {
        _classToPrint = classToPrint;
    }
    public IEnumerable<T> Items
    {
        get => _classToPrint.GetElements;
    }
    public int Count
    {
        get => _classToPrint.GetElements.Count();
    }
}