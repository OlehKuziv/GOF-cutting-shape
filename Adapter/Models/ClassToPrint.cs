using Adapter.Interfaces;

namespace Adapter.Models;

public class ClassToPrint:IElements<int>
{
    private readonly IEnumerable<int> _elements;

    public ClassToPrint(IEnumerable<int> elements)
    {
        _elements = elements;
    }
    public IEnumerable<int> GetElements
    {
        get => _elements;
    }
}