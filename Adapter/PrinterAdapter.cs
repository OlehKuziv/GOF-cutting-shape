using Adapter._3partyLib;
using Adapter.Interfaces;
using Adapter.Models;

namespace Adapter;

public class PrinterAdapter:IElementsPrinter
{
    private readonly Printer _printer;

    public PrinterAdapter(Printer printer)
    {
        _printer = printer;
    }

    public void Print<T>(IElements<T> elements)
    {
        ContainerWrapper<T> containerWrapper = new ContainerWrapper<T>(elements); 
        _printer.Print(containerWrapper);
    } 
}