﻿using Adapter._3partyLib;
using Adapter.Interfaces;
using Adapter.Models;

namespace Adapter;

public class Program
{
    static void Main()
    {
        IEnumerable<int> items = Enumerable.Range(0, 99);
        ClassToPrint classToPrint = new ClassToPrint(items);

        Printer printer = new Printer();
        IElementsPrinter printerAdapter = new PrinterAdapter(printer);
        printerAdapter.Print(classToPrint);
    }
}