namespace Adapter.Interfaces;

public interface IElements<T> 
{
    IEnumerable<T> GetElements { get; }
}