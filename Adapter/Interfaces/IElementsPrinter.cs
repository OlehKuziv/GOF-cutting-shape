namespace Adapter.Interfaces;

public interface IElementsPrinter
{
     void Print<T>(IElements<T> elements);
}